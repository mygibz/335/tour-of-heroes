import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Hero } from './hero';
import { MessageService } from './message.service';

import { HEROES } from "./mock-heroes";

@Injectable({ providedIn: 'root' })
export class HeroService {

  constructor(
    private messageService: MessageService) { }

  private FillLocalStorageIfNecesary(): void {
    if (!localStorage.getItem('heroes')) {
      localStorage.setItem('heroes', JSON.stringify(HEROES));
      this.log(`reset heroes`);
      console.log(`reset heroes`);
    }
  }

  getHeroes(hideMessage = false): Hero[] {
    this.FillLocalStorageIfNecesary();
    if (!hideMessage)
      this.log(`fetched heroes`);
    return JSON.parse(localStorage.getItem('heroes'));
  }

  getHero(id: number): Observable<Hero> {
    this.FillLocalStorageIfNecesary();
    this.log(`fetched hero id=${id}`);
    return of(this.getHeroes().find(hero => hero.id == id));
  }

  updateHero(hero: Hero) {
    this.log(`updated hero id=${hero.id}`);

    const HEROES: Hero[] = this.getHeroes(true);

    HEROES.forEach(element => {
      if (element.id == hero.id)
        element.name = hero.name;
    });

    localStorage.setItem('heroes', JSON.stringify(HEROES));

    window.location.replace("/heroes");
  }


  addHero(name: string): number{
    const HEROES: Hero[] = this.getHeroes(true);

    const newID = HEROES[HEROES.length - 1].id +1;
    const newHero : Hero = {id: newID, name}

    HEROES.push(newHero);
    this.log(`added hero w/ id=${newHero.id}`);

    localStorage.setItem('heroes', JSON.stringify(HEROES));

    return newHero.id;
  }

  deleteHero(hero: Hero) {
    this.log(`removed hero id=${hero.id}`);

    const HEROES: Hero[] = this.getHeroes(true);

    for (let i = 0; i < HEROES.length; i++)
      if (HEROES[i].id == hero.id)
        HEROES.splice(i, 1);

    localStorage.setItem('heroes', JSON.stringify(HEROES));

    window.location.replace("/heroes");
  }

  searchHeroes(term: string): Hero[] {
    const HEROES: Hero[] = this.getHeroes(true);
    var foundHeroes: Hero[] = [];
    HEROES.forEach(element => {
      if (element.name.toLowerCase().includes(term.toLowerCase()))
        foundHeroes.push(element)
    });

    if (term != "")
      this.log(`searched heros for term=${term}`);
    else
      this.log(`listed all heroes`);

    return foundHeroes;
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }
}